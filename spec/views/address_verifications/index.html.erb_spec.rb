require 'rails_helper'

RSpec.describe "address_verifications/index", type: :view do
  before(:each) do
    assign(:address_verifications, [
      AddressVerification.create!(
        :user_id => 2,
        :country_id => 3,
        :city => "City",
        :state => "State",
        :street => "Street",
        :building => "Building",
        :number => "Number",
        :status => 4,
        :note => "Note"
      ),
      AddressVerification.create!(
        :user_id => 2,
        :country_id => 3,
        :city => "City",
        :state => "State",
        :street => "Street",
        :building => "Building",
        :number => "Number",
        :status => 4,
        :note => "Note"
      )
    ])
  end

  it "renders a list of address_verifications" do
    render
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => 3.to_s, :count => 2
    assert_select "tr>td", :text => "City".to_s, :count => 2
    assert_select "tr>td", :text => "State".to_s, :count => 2
    assert_select "tr>td", :text => "Street".to_s, :count => 2
    assert_select "tr>td", :text => "Building".to_s, :count => 2
    assert_select "tr>td", :text => "Number".to_s, :count => 2
    assert_select "tr>td", :text => 4.to_s, :count => 2
    assert_select "tr>td", :text => "Note".to_s, :count => 2
  end
end
