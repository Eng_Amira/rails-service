require 'rails_helper'

RSpec.describe 'Users API', type: :request do
  let(:user) { build(:user) }
  let(:headers) { valid_headers.except('Authorization') }
  let(:valid_attributes) do
    attributes_for(:user, password_confirmation: user.password)
  end


  # Test suite for POST /users
  describe 'POST /api/v1/users/create' do
    # valid payload
    let(:country) { Country.create(short_code: 'EGP', Phone_code: '057', Full_Name: 'EGYPT', Currency: 'EGP', language: 'Arabic', active: '1') }
    let(:country_id) { country.id }
    # Create test user
    let(:valid_attributes) { { username: 'Amira' ,password: 'Amira123456', email: 'amira@yahoo.com' , country_id: country_id} }
      
    context 'when the request is valid' do
    
      before { post '/api/v1/users/create', params: {user: valid_attributes }}
      
      it 'creates a user' do
        expect(json['username']).to eq('Amira')
      end

      it 'returns status code 201' do
        expect(response).to have_http_status(201)
      end
    end

    context 'when the request is invalid' do
      before { post '/api/v1/users/create', params: {user: {firstname: 'amira'} } }

      it 'returns status code 422' do
        expect(response).to have_http_status(422)
      end

      it 'returns a validation failure message' do
        expect(response.body)
        .to match("{\"email\":[\"is invalid\",\"can't be blank\"],\"password\":[\"can't be blank\"],\"country\":[\"must exist\"],\"username\":[\"can't be blank\"],\"country_id\":[\"is invalid\"]}") 
      end
    end
  end

  



end