class EmailConfirmationGuard < Clearance::SignInGuard
    def call
      if unconfirmed?
        failure("You must confirm your email address.")
      else
        failure
        #next_guard
      end
    end
  
    def unconfirmed?
      #signed_in? && !current_user.confirmed_at
      signed_in? && current_user.status == 0
    end
  end
  

  #def call
   # if !signed_in?
    #  next_guard
    #elseif current_user.active?
     # next_guard
    #else
     # fail("Some message.")
    #end
  #end