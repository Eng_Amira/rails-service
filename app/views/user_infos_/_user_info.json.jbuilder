json.extract! user_info, :id, :user_id, :issue_date, :expire_date, :nationalid_no, :name, :address, :created_at, :updated_at
json.url user_info_url(user_info, format: :json)
