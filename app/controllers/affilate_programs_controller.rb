class AffilateProgramsController < ApplicationController
  before_action :set_affilate_program, only: [:show, :edit, :update, :destroy]
  before_action :require_login , except: [:set_invitation_code]
  skip_before_action :check2fa ,only: [:set_invitation_code]
  skip_before_action :check_active_session ,only: [:set_invitation_code]

  # show list of invitations
  # @return [Integer] user_id
  # @return [Integer] refered_by
  def index
    @affilate_programs = AffilateProgram.joins(:user).all
  end

  # GET /affilate_programs/1
  # GET /affilate_programs/1.json
  def show
  end

  # GET /affilate_programs/new
  def new
    @affilate_program = AffilateProgram.new
  end

  # GET /affilate_programs/1/edit
  def edit
  end

  # POST /affilate_programs
  # POST /affilate_programs.json
  def create
    @affilate_program = AffilateProgram.new(affilate_program_params)

    respond_to do |format|
      if @affilate_program.save
        format.html { redirect_to @affilate_program, notice: 'Affilate program was successfully created.' }
        format.json { render :show, status: :created, location: @affilate_program }
      else
        format.html { render :new }
        format.json { render json: @affilate_program.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /affilate_programs/1
  # PATCH/PUT /affilate_programs/1.json
  def update
    respond_to do |format|
      if @affilate_program.update(affilate_program_params)
        format.html { redirect_to @affilate_program, notice: 'Affilate program was successfully updated.' }
        format.json { render :show, status: :ok, location: @affilate_program }
      else
        format.html { render :edit }
        format.json { render json: @affilate_program.errors, status: :unprocessable_entity }
      end
    end
  end

  # when invited users click on invitation link which send to their mails,
  # they are redirected here and invitation code put in the session to be saved during registeration
  # @param [Integer] refered_by
  def set_invitation_code
    session[:refered_by] = params[:refered_by]
    redirect_to sign_up_path
  end


  # registered users can send invitation mail to other users
  # @param [String] email
  def send_invitation_email
    @email_to = params[:invitiation][:email]
    UserMailer.invitation(@email_to,current_user).deliver_later
    redirect_to current_user , notice: "invitation send" 
  end

  # DELETE /affilate_programs/1
  # DELETE /affilate_programs/1.json
  def destroy
    @affilate_program.destroy
    respond_to do |format|
      format.html { redirect_to affilate_programs_url, notice: 'Affilate program was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_affilate_program
      @affilate_program = AffilateProgram.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def affilate_program_params
      params.require(:affilate_program).permit(:user_id, :refered_by)
    end
end
