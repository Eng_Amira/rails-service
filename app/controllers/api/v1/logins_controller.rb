class Api::V1::LoginsController < ApplicationController
  #before_action :require_login
  before_action :set_login, only: [:show, :edit, :update, :destroy]
  #before_action :authenticate_request!
  skip_before_action :verify_authenticity_token
  attr_reader :current_user

  # GET /logins
  # GET /logins.json
  def index
    if current_user.roleid == 1
      @logins = Login.all
      else
        redirect_to root_path , notice: "not allowed" 
    end   
  end


      # show user's active sessions
      # GET /active_sessions
      #=begin
      # @api {get} /api/v1/active_sessions?id={:user-id} show user's active sessions
      # @apiVersion 0.3.0
      # @apiName Getactivesessions
      # @apiGroup Log
      # @apiDescription show user's active sessions.
      # @apiExample Example usage:
      # curl -i http://localhost:3000/api/v1/active_sessions?id=1 -H "Authorization: eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjo5fQ.xM_NrnhjjJejtZFatkVZWAZS_BFxvtjBns50IdcakzU"
      #
      # @apiSuccess {Integer}     id                    Users-ID.
      # @apiSuccess {Integer}     ipaddress             Ip address
      # @apiSuccess {String}      user_agent            Session Details	.
      # @apiSuccess {String}      device_id             device id
      # @apiSuccess {Datetime}    created_at            Date of creating the session.
      # @apiSuccess {Datetime}    updated_at            Date of updating the session.
      #
      # @apiError NoAccessRight Only authenticated users can perform this action.
      #
      # @apiErrorExample Response (example):
      #     HTTP/1.1 401 Unauthorized
      #     {
      #       "error": "NoAccessRight"
      #    }
      #=end

  def active_sessions
      @logins = Login.where("user_id =? ",params[:id])
      respond_to do |format|
          if current_user.roleid == 1 or current_user.id == params[:id].to_i
             format.json { render json: @logins }
          else
              format.json { render json: "Not Allowed" }
          end 
      end
  end

  # GET /logins/1
  # GET /logins/1.json
  def show
    if current_user.roleid != 1 and current_user.id != params[:id].to_i 
      redirect_to root_path , notice: "not allowed" 
    end
  end

  # GET /logins/new
  def new
    if current_user.roleid == 1 
      @login = Login.new
    else
      redirect_to root_path , notice: "not allowed" 
    end
    
  end

  # GET /logins/1/edit
  def edit
    if current_user.roleid != 1 
      redirect_to root_path , notice: "not allowed" 
    end
  end

  # POST /logins
  # POST /logins.json
  def create
    @login = Login.new(login_params)

    respond_to do |format|
      if @login.save
        format.html { redirect_to @login, notice: 'Login was successfully created.' }
        format.json { render :show, status: :created, location: @login }
      else
        format.html { render :new }
        format.json { render json: @login.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /logins/1
  # PATCH/PUT /logins/1.json
  def update
    if current_user.roleid != 1 
      redirect_to root_path , notice: "not allowed" 
    else
     respond_to do |format|
       if @login.update(login_params)
         format.html { redirect_to @login, notice: 'Login was successfully updated.' }
         format.json { render :show, status: :ok, location: @login }
       else
         format.html { render :edit }
         format.json { render json: @login.errors, status: :unprocessable_entity }
       end
     end
    end
  end

  # DELETE /logins/1
  # DELETE /logins/1.json
  def destroy
    if current_user.roleid != 1 
      redirect_to root_path , notice: "not allowed" 
    else
      @login.destroy
      cookies.delete(:auth_token)
      cookies.delete(:device_id)
      sign_out
      respond_to do |format|
        format.html { redirect_to logins_url, notice: 'Login was successfully destroyed.' }
        format.json { head :no_content }
      end
    end      
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_login
      @login = Login.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def login_params
      params.require(:login).permit(:user_id, :ip_address, :user_agent, :device_id)
    end
end
