class Api::V1::AffilateProgramsController < ApplicationController
  before_action :set_affilate_program, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_request!
  skip_before_action :verify_authenticity_token
  attr_reader :current_user

  # GET /affilate_programs
  # GET /affilate_programs.json
  def index
    @affilate_programs = AffilateProgram.all
  end

  # GET /affilate_programs/1
  # GET /affilate_programs/1.json
  def show
  end

  # GET /affilate_programs/new
  def new
    @affilate_program = AffilateProgram.new
  end

  # GET /affilate_programs/1/edit
  def edit
  end

  # POST /affilate_programs
  # POST /affilate_programs.json
  def create
    @affilate_program = AffilateProgram.new(affilate_program_params)

    respond_to do |format|
      if @affilate_program.save
        format.html { redirect_to @affilate_program, notice: 'Affilate program was successfully created.' }
        format.json { render :show, status: :created, location: @affilate_program }
      else
        format.html { render :new }
        format.json { render json: @affilate_program.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /affilate_programs/1
  # PATCH/PUT /affilate_programs/1.json
  def update
    respond_to do |format|
      if @affilate_program.update(affilate_program_params)
        format.html { redirect_to @affilate_program, notice: 'Affilate program was successfully updated.' }
        format.json { render :show, status: :ok, location: @affilate_program }
      else
        format.html { render :edit }
        format.json { render json: @affilate_program.errors, status: :unprocessable_entity }
      end
    end
  end

  def send_invitation
    session[:refered_by] = params[:refered_by]
    redirect_to sign_up_path
  end


    # POST /affilate_programs.json
    # =begin
    # @api {post} /affilate_programs/ Send Invitation Email
    # @apiVersion 0.3.0
    # @apiName postAffilateProgram
    # @apiGroup AffilateProgram
    # @apiDescription Send Invitation Email.
    # @apiParam {String}     email                Email of the invited user.
    #
    # @apiExample Example usage:       
    # curl -X POST  http://localhost:3000/api/v1/send_invitation_email -H 'cache-control: no-cache' -H 'content-type: application/json' -H "Authorization: eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxMX0.ikUiajq9UFTi4OJDDGR2Xqk79U5vtiRWJaa8HgneqC4" -d '{"email": "engamira333@gmail.com"}' 
    #
    # @apiSuccessExample Response (example):
    #     HTTP/ 200 OK
    #     {
    #       "result": "true"
    #    }
    #
    # @apiError MissingToken Only authenticated users can send invitation.  
    # @apiError MissingData Email can not be empty.
    #
    # @apiErrorExample Error-Response:
    #     HTTP/1.1 400 Bad Request
    #     {
    #       "result : false"
    #       "error": "Not Authenticated"
    #       "status": 400- Bad Request
    #    }
    # @apiErrorExample Error-Response2:
    #     HTTP/1.1 400 Bad Request
    #     {
    #       "result : false"
    #       "error": "MissingData"
    #       "status": 400- Bad Request
    #    }
    # =end

  def send_invitation_email
    @email_to = params[:email]
    if (@email_to != nil and @email_to != "")
       UserMailer.invitation(@email_to,current_user).deliver_later
       render json: {'result' => true}
    else
       render json: {'result' => false, 'error' => 'MissingData'}
    end
  end

  # DELETE /affilate_programs/1
  # DELETE /affilate_programs/1.json
  def destroy
    @affilate_program.destroy
    respond_to do |format|
      format.html { redirect_to affilate_programs_url, notice: 'Affilate program was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_affilate_program
      @affilate_program = AffilateProgram.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def affilate_program_params
      params.require(:affilate_program).permit(:user_id, :refered_by)
    end
end
