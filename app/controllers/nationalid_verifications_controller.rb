class NationalidVerificationsController < ApplicationController
  before_action :set_nationalid_verification, only: [:show, :edit, :update, :destroy]
  before_action :require_login


  # GET /nationalid_verifications/1
  # GET /nationalid_verifications/1.json
  def show
    if current_user.id != @nationalid_verification.user_id.to_i 
      redirect_to root_path , notice: "not allowed" 
    end
  end

  # create new National Id Verification
  # each user can create only one National Id verification
  def new
    @current_user_verification = NationalidVerification.where("user_id =?", current_user.id.to_i).first
    if @current_user_verification != nil
      redirect_to root_path , notice: "not allowed"
    else
      @nationalid_verification = NationalidVerification.new
    end
  end

  # edit National Id Verification
  # user can edit his national id verification only if status is not verified
  # @param [Integer] id
  def edit
    if (@nationalid_verification.status == "Verified") or (current_user.id != @nationalid_verification.user_id.to_i)
      redirect_to root_path , notice: "not allowed" 
    end
  end

  # create new National Id Verification
  # @param [String] legal_name
  # @param [Integer] national_id
  # @param [Integer] document_type
  # @param [Date] issue_date
  # @param [Date] expire_date
  # @param [Blob] attachment1
  # @param [Blob] attachment2
  # @return [Integer] id
  # @return [String] username
  # @return [String] legal_name
  # @return [Integer] national_id
  # @return [Integer] document_type
  # @return [Date] issue_date
  # @return [Date] expire_date
  # @return [Blob] attachment1
  # @return [Blob] attachment2
  # @return [Integer] status
  # @return [datetime] created_at
  # @return [datetime] updated_at
  def create
    @nationalid_verification = NationalidVerification.new(nationalid_verification_params)
    @nationalid_verification.user_id = current_user.id
    @nationalid_verification.status = 0
    respond_to do |format|
      if @nationalid_verification.save
        format.html { redirect_to @nationalid_verification, notice: 'Nationalid verification was successfully created.' }
        format.json { render :show, status: :created, location: @nationalid_verification }
      else
        format.html { render :new }
        format.json { render json: @nationalid_verification.errors, status: :unprocessable_entity }
      end
    end
  end

  # edit National Id Verification,
  # only admins can edit status and note.
  # @param [String] legal_name
  # @param [Integer] national_id
  # @param [Integer] document_type
  # @param [Date] issue_date
  # @param [Date] expire_date
  # @param [Blob] attachment1
  # @param [Blob] attachment2
  # @param [Integer] status
  # @param [String] note
  # @return [Integer] id
  # @return [String] username
  # @return [String] legal_name
  # @return [Integer] national_id
  # @return [Integer] document_type
  # @return [Date] issue_date
  # @return [Date] expire_date
  # @return [Blob] attachment1
  # @return [Blob] attachment2
  # @return [Integer] status
  # @return [String] note
  # @return [datetime] created_at
  # @return [datetime] updated_at
  def update
    respond_to do |format|
      if @nationalid_verification.update(nationalid_verification_params)
        format.html { redirect_to @nationalid_verification, notice: 'Nationalid verification was successfully updated.' }
        format.json { render :show, status: :ok, location: @nationalid_verification }
      else
        format.html { render :edit }
        format.json { render json: @nationalid_verification.errors, status: :unprocessable_entity }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_nationalid_verification
      @nationalid_verification = NationalidVerification.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def nationalid_verification_params
      params.require(:nationalid_verification).permit(:user_id, :legal_name, :national_id, :document_type, :issue_date, :expire_date, :status, :note, :attachment1, :attachment2, images: [])
    end
end
