class UsersController < ApplicationController
  before_action :require_login , only: [:show, :edit, :update, :destroy,:index, :homepage, :add_admin, :add_admin_post]
  before_action :set_user, only: [:show, :edit, :update, :destroy]
  skip_before_action :check2fa ,only: [:new, :destroy, :create]
  skip_before_action :check_active_session ,only: [:new, :destroy, :create]
  
  include Clearance::Authentication 
  #before_action :get_google_auth ,only: [:show, :edit, :update,:index]
  require 'rqrcode'

  
  # show data of user
  # @param [Integer] id 
  # @return [String] username
  # @return [String] email
  # @return [String] account_number
  # @return [datetime] created_at
  # @return [datetime] updated_at
  def show
    if current_user.id == params[:id].to_i
      @user = User.where("id =? " ,params[:id]).first
      @qr = RQRCode::QRCode.new(@user.provisioning_uri("payers"), :size => 7, :level => :h )
    else
      redirect_to root_path , notice: "not allowed" 
    end
  end
  
  # when activating google authentication, if there is a problem send the code to user's mail
  # send confirmation email if google authentication has a problem
  def send_confirmation_email
    @token = SecureRandom.hex(4)
    session[:token_code] = @token
    UserMailer.confirmsignin(current_user,@token).deliver_later
    redirect_to two_factor_path
  end
 
  def homepage   
    @all_users = User.all
    @count_admins = @all_users.where("roleid = ?",1).count
    @count_users = @all_users.where("roleid = ?",2).count
    @disabled_users = @all_users.where("disabled = ?",1).count
  end

  # after number of failed login attempts, user's account will be disabled and link will be send to user's mail. 
  # when the user click the link, he is redirected here to unlock account and allow him to signin again. 
  # @param [Integer] user_id 
  # @param [String] token 
  def unlockaccount
    @id = params[:id]
    @token = params[:token]
    @user = User.where("id =?",@id ).first
    if @token  == @user.unlock_token      
       @user.failedattempts = 0
       @user.disabled = 0
       @user.unlock_token = ""
       @user.save
       flash.now.notice = 'Your account is Unlocked,please sign in .'
       render template: "sessions/new"  
    else
      flash.now.notice = 'InValid Token .'
      render template: "sessions/new"  
    end
  end

  # confirm user mail after registeration
  # @param [Integer] user_id 
  # @param [String] token
  # when user signup, a confirmation link is send to his email .
  # when the user click the link, he is redirected here to confirm account and signin
  def confirmmail
    @id = params[:id]
    @token = params[:token]
    @user = User.where("id =?",@id ).first
    @verify_code = Verification.where("user_id =?",@id ).first
    if @verify_code.email_confirmation_token == @token
      @user.update(:status => 1)
      @verify_code.update(:email_confirmed_at => Time.now)      
    end
    redirect_to sign_in_path
  end

  # form to create new user
  def new
    if session[:refered_by] != nil
      @refered_by = session[:refered_by]
    end
    @user = User.new
    render :layout => false
  end

  # edit user account
  def edit
    if current_user.roleid != 1 and current_user.id != params[:id].to_i 
      redirect_to root_path , notice: "not allowed" 
    end
  end

  # create new account
  # @param [String] username 
  # @param [String] email
  # @param [String] password 
  # @param [Integer] country_id 
  # @param [String] refered_by 
  # @return [String] username 
  # @return [String] email
  # @return [String] password 
  # @return [Integer] country_id 
  # @return [String] uuid
  # @return [String] remember_token
  # @return [String] account_number
  # @return [String] invitation_code
  # @return [Integer] active_otp
  # @return [boolean] account_currency
  # @return [datetime] created_at
  # @return [datetime] updated_at
  def create
    @user = User.new(user_params)
    #@user.uuid = SecureRandom.hex(4)
    #@user.invitation_code = SecureRandom.hex(4)
    #@user.auth_token = SecureRandom.hex(6)
    @token = SecureRandom.hex(4)
    respond_to do |format|
      if @user.save
        @acc_num =  "PS1" + "%08d" % @user.id.to_i 
        @user.update(:account_number => @acc_num)
        Verification.create(:user_id => @user.id,:email_confirmation_token => @token)
        Watchdog.create(:user_id => @user.id,:ipaddress => request.env['REMOTE_ADDR'],:logintime => Time.now,:lastvisit => Time.now ,:operation_type => "sign_up")
        if (session[:refered_by] != nil )
          @invited_by = User.where("invitation_code =? ",session[:refered_by]).first
          if (@invited_by != nil)
             AffilateProgram.create(:user_id => @user.id, :refered_by => @invited_by.id)
          end
        end
        UserMailer.confirmmail(@user,@token).deliver_later
        format.html { redirect_to sign_in_path	, notice: 'User was successfully created,You should confirm your email before sign in' }
        format.json { render :show, status: :created, location: @user }
      else
        format.html { render :new }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # after user sign in, he redirected to this page to confirm signin.
  # user should enter the code which has been sent to his email or his google code.
  def get_two_factor
    session[:verify_code] = false
    render :layout => false
  end

  # to check the correctness of the entered code.
  # @param [String] code
  # @param [String] token_code
  def post_two_factor
    @user = current_user 
    session[:verify_code] = false
    @user_code = params[:confirm][:code]
    @sent_code = session[:token_code]
    if (@user.active_otp == 1  and  @user_code == @sent_code) or (@user.active_otp == 2 and (@user.authenticate_otp(@user_code, drift: 60) or @user_code == @sent_code))
      
         session[:verify_code] = true
         redirect_to @user, notice: "Welcome back!"
    else 
        redirect_to two_factor_path, notice: "wrong code!" 
    end    
    
  end

  # update data of a user .
  # edit users data, only admins and account owner can edit this data.
  # @param [Integer] username 
  # @param [Integer] email
  # @param [password] password 
  # @param [Integer] country_id 
  # @param [Integer] firstname
  # @param [Integer] lastname
  # @param [Integer] active_otp
  # @param [boolean] account_currency
  # @param [password] confirm_password 
  # @return [Integer] username 
  # @return [Integer] email
  # @return [Integer] password 
  # @return [Integer] country_id 
  # @return [Integer] firstname
  # @return [Integer] lastname 
  # @return [String] uuid
  # @return [String] remember_token
  # @return [String] account_number
  # @return [String] invitation_code
  # @return [Integer] active_otp
  # @return [boolean] account_currency
  def update
    if !(params[:user][:active_otp])
        @user = User.authenticate(current_user.email,params[:user][:confirm_password])
        if !(@user)
             redirect_to edit_user_path(params[:id]) , notice: "wrong password confirmation" and return
        end
    end
    if current_user.id == params[:id].to_i
     respond_to do |format|
        if @user.update(user_params)
          format.html { redirect_to @user, notice: 'User was successfully updated.' }
          format.json { render :show, status: :ok, location: @user }
        else
          #if (params[:user][:password])
           #  format.html { render :edit_password }
          #else
            format.html { render :edit }
          #end
          format.json { render json: @user.errors, status: :unprocessable_entity }
        end
      end
    else
      redirect_to root_path , notice: "not allowed" 
    end    
  end

  # edit user password
  def edit_password
    @user = User.where("id =?", params[:id]).first
  end
  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    if current_user.roleid == 1
      @user.destroy
      respond_to do |format|
        format.html { redirect_to users_url, notice: 'User was successfully destroyed.' }
        format.json { head :no_content }
      end
    else
      redirect_to root_path , notice: "not allowed" 
    end
  end


  # on change confirmation way invoke this function to make changes
  # if mail confirmation is selected, send token to user mail, to confirm changes
  # if google authentacation has been selected, generate new QR,user read it and enter qrcode to comfirm changes
  # @param [String] active_otp
  def change_confirmation_code
    if params[:id] == "mail"
    @token = SecureRandom.hex(4)
    session[:token_code] = @token
    UserMailer.confirmsignin(current_user,@token).deliver_later
    @user = current_user
    @user.update_attribute(:otp_secret_key, '')
    render :json => { 'result': @token }
    elsif params[:id] == "google"
      @user = current_user
      @user.update_attribute(:otp_secret_key, ROTP::Base32.random_base32)
      @qr = RQRCode::QRCode.new(@user.provisioning_uri("payers"), :size => 7, :level => :h )
      render :json => { 'result': @qr  , 'otp_secret_key': @user.otp_secret_key}
    elsif params[:id] == "nochangemail"
      @user = current_user
      @user.update_attribute(:otp_secret_key, '')
    end
  end


  # check correctnes of the code in case of google code
  # @param  [String] code
  # @return [boolean] result
  def confirm_google_code
    @user = current_user
    @user_code = params[:code]
    if (@user.authenticate_otp(@user_code, drift: 60))
       @result = true
    else
      @result = false
    end 
    render :json => { 'result': @result }
  end



  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:user).permit(:uuid, :roleid,  :firstname, :lastname, :language, :disabled, :loginattempts, :username, :email, :password, :secret_code, :otp_secret_key, :active_otp, :account_number, :telephone, :account_currency, :country_id, :unlock_token, :refered_by, :avatar)
      
    end
end
