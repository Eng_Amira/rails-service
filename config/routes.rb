Rails.application.routes.draw do



  resources :selfie_verifications, :except => [:index, :destroy]
  resources :address_verifications, :except => [:index, :destroy]
  resources :nationalid_verifications, :except => [:index, :destroy]
  resources :user_infos, :only => [:show, :create, :update]
  namespace :api, defaults: {format: 'json'} do
    namespace :v1 do
      resources :watchdogs
      resources :users
      resources :countries
      resources :affilate_programs
      post "users/create" => "users#create"
      get "homepage" => "users#homepage"
      post "confirmmail" => "users#confirmmail"
      post "resend_confirmmail" => "users#resend_confirmmail"
      get "two_factor" => "users#get_two_factor"
      post "two_factor" => "users#post_two_factor"
      get "send_confirmation_email" => "users#send_confirmation_email"
      get "change_confirmation_code" => "users#change_confirmation_code"
      get "confirm_google_code" => "users#confirm_google_code"
      get "user_log" => "watchdogs#user_log"
      get "active_sessions" => "logins#active_sessions"
      post 'auth_user' => 'authentication#authenticate_user'
      post "unlockaccount" => "authentication#unlockaccount"
      post "send_invitation_email" => "affilate_programs#send_invitation_email"
      post 'getEditToken' => 'authentication#getEditToken'
    end
  end



  resources :affilate_programs, :only => [:create, :destroy]
  resources :countries, :only => [:index, :show]
  resources :logins, :only => [:create, :show, :destroy]
  resources :verifications, :only => [:create, :update, :destroy]
  resources :watchdogs, :only => [:create, :show, :destroy]
  resources :users, :except => [:index]
  root "users#homepage"
  get "confirmmail" => "users#confirmmail"
  get "unlockaccount" => "users#unlockaccount"
  get "two_factor" => "users#get_two_factor"
  post "two_factor" => "users#post_two_factor"
  get "user_log" => "watchdogs#user_log"
  get "active_sessions" => "logins#active_sessions"
  get "send_confirmation_email" => "users#send_confirmation_email"
  get "change_confirmation_code" => "users#change_confirmation_code"
  get "confirm_google_code" => "users#confirm_google_code"
  #get "set_otp_code" => "users#set_otp_code"
  get "affilate-program/:refered_by" => "affilate_programs#set_invitation_code"
  post "send_invitation_email" => "affilate_programs#send_invitation_email"
  get "edit_password/:id" => "users#edit_password" , :as => "edit_password" 
  get "error_page" => "logins#error_page"
  get "lock_page" => "logins#lock_page"
  post "lock_page" => "logins#check_to_unlock"
  
  #match '*path', to: "logins#error_page", via: :all



  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
