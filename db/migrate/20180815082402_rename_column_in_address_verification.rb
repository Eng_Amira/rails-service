class RenameColumnInAddressVerification < ActiveRecord::Migration[5.2]
  def change
    rename_column :address_verifications, :building, :apartment_number
  end
end
