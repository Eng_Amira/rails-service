class Createuserinfos < ActiveRecord::Migration[5.2]
  def change
    create_table :user_infos do |t|
      t.integer :user_id
      t.string :mobile
      t.integer :address_verification_id
      t.integer :nationalid_verification_id
      t.integer :selfie_verification_id
      t.integer :status
      t.string :name

      t.timestamps
    end
  end
end
