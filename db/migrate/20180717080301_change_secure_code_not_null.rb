class ChangeSecureCodeNotNull < ActiveRecord::Migration[5.2]
  def change
    User.all.each { |user| user.update_attribute(:secret_code, "123456") }
    change_column_null :users, :secret_code, false
    
  end
end
