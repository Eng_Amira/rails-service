# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_09_17_130623) do

  create_table "active_storage_attachments", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.integer "record_id", null: false
    t.integer "blob_id", null: false
    t.datetime "created_at", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.bigint "byte_size", null: false
    t.string "checksum", null: false
    t.datetime "created_at", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "address_verifications", force: :cascade do |t|
    t.integer "user_id"
    t.integer "country_id"
    t.string "city"
    t.string "state"
    t.string "street"
    t.string "apartment_number"
    t.integer "status", default: 0, null: false
    t.string "note"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "admins", force: :cascade do |t|
    t.string "uuid"
    t.integer "roleid", default: 1
    t.string "username"
    t.string "firstname"
    t.string "lastname"
    t.string "language"
    t.boolean "disabled", default: false, null: false
    t.integer "loginattempts", default: 0, null: false
    t.boolean "status", default: false, null: false
    t.integer "failedattempts", default: 0, null: false
    t.string "secret_code"
    t.string "otp_secret_key"
    t.integer "active_otp", default: 1, null: false
    t.string "account_number"
    t.string "telephone"
    t.boolean "account_currency", default: false
    t.integer "country_id"
    t.string "invitation_code"
    t.string "unlock_token"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "affilate_programs", force: :cascade do |t|
    t.integer "user_id"
    t.integer "refered_by"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "countries", force: :cascade do |t|
    t.string "short_code"
    t.string "Full_Name"
    t.string "Phone_code", limit: 10
    t.string "Currency"
    t.string "language"
    t.integer "active"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "logins", force: :cascade do |t|
    t.integer "user_id"
    t.string "ip_address"
    t.string "user_agent"
    t.string "device_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "operation_type"
    t.index ["device_id"], name: "index_logins_on_device_id"
    t.index ["user_id"], name: "index_logins_on_user_id"
  end

  create_table "nationalid_verifications", force: :cascade do |t|
    t.integer "user_id", null: false
    t.string "legal_name"
    t.string "national_id"
    t.integer "document_type"
    t.date "issue_date"
    t.date "expire_date"
    t.integer "status", default: 0
    t.string "note"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "selfie_verifications", force: :cascade do |t|
    t.integer "user_id"
    t.string "note"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "status", default: 0, null: false
  end

  create_table "user_infos", force: :cascade do |t|
    t.integer "user_id"
    t.string "mobile"
    t.integer "address_verification_id"
    t.integer "nationalid_verification_id"
    t.integer "selfie_verification_id"
    t.integer "status", default: 0, null: false
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string "uuid"
    t.integer "roleid", default: 2
    t.string "username"
    t.string "firstname"
    t.string "lastname"
    t.string "language"
    t.integer "disabled", default: 0, null: false
    t.integer "loginattempts", default: 0, null: false
    t.integer "status", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "email"
    t.string "encrypted_password", limit: 128
    t.string "confirmation_token", limit: 128
    t.string "remember_token", limit: 128
    t.integer "failedattempts", default: 0, null: false
    t.string "auth_token"
    t.string "secret_code"
    t.string "otp_secret_key"
    t.integer "active_otp", default: 1
    t.string "account_number"
    t.string "telephone"
    t.boolean "account_currency", default: false
    t.integer "country_id"
    t.string "invitation_code"
    t.string "unlock_token"
    t.integer "shipping_adreess_id", default: 0
    t.index ["email"], name: "index_users_on_email"
    t.index ["remember_token"], name: "index_users_on_remember_token"
  end

  create_table "verifications", force: :cascade do |t|
    t.integer "user_id"
    t.text "email_confirmation_token"
    t.datetime "email_confirmed_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "watchdogs", force: :cascade do |t|
    t.integer "user_id"
    t.datetime "logintime"
    t.text "ipaddress"
    t.datetime "lastvisit"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "operation_type"
  end

end
